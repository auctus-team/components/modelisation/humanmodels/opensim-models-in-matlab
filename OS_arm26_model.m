% Creation of a simple 2 dofs and 6 muscles opensim model  
import org.opensim.modeling.*;

% Build a model
model = Model();

radius = 0.05;
halfheight = 0.5;
mass = 1;
massCenter = Vec3(0,0,0);
inertia = Inertia(mass/12*(3*radius^2 + halfheight^2), 0.5*mass*radius^2, mass/12*(3*radius^2 + halfheight^2), 0, 0, 0);

% Body n1
body1 = Body();
body1.setName('body1');
body1.setMass(mass);
body1.setMassCenter(massCenter);
body1.setInertia(inertia);
body1.attachGeometry(Cylinder(radius, halfheight));
model.addBody(body1);

% Body n2
body2 = Body();
body2.setName('body2');
body2.setMass(mass);
body2.setMassCenter(massCenter);
body2.setInertia(inertia);
body2.attachGeometry(Cylinder(radius, halfheight));
model.addBody(body2);

%Create joint 1
locInParent = Vec3(0,0,0); % Location of the joint origin expressed in the parent frame
oriInParent = Vec3(0,0,0); % Orientation of the joint frame in the parent frame
locInChild = Vec3(0,halfheight,0); % Location of the joint origin expressed in the child frame
oriInChild = Vec3(0,0,0); % Orientation of the joint frame in the child frame
joint1 = PinJoint('joint1', model.getGround(), locInParent, oriInParent, body1, locInChild, oriInChild); % Construct the joint;

coord1 = joint1.updCoordinate();
coord1.setName('rotation1');
coord1.setDefaultValue(20*pi/180);
model.addJoint(joint1);

%Create joint 2
locInParent = Vec3(0,-halfheight,0); % Location of the joint origin expressed in the parent frame
oriInParent = Vec3(0,0,0); % Orientation of the joint frame in the parent frame
locInChild = Vec3(0,halfheight*0.6,0); % Location of the joint origin expressed in the child frame
oriInChild = Vec3(0,0,0); % Orientation of the joint frame in the child frame
joint2 = PinJoint('joint2', body1, locInParent, oriInParent, body2, locInChild, oriInChild); % Construct the joint;

coord2 = joint2.updCoordinate();
coord2.setName('rotation2');
coord2.setDefaultValue(40*pi/180);
model.addJoint(joint2);

% add muscles
%% Define Muscles in the Model
% Define parameters for a Muscle
maxIsometricForce  = 1000.0;
optimalFiberLength = 0.4;
tendonSlackLength  = 0.1;
pennationAngle 	   = 0.0;

% Instantiate Muscle 1
muscle1 = Thelen2003Muscle();
muscle1.setName('muscle1');
muscle1.setMaxIsometricForce(maxIsometricForce);
muscle1.setOptimalFiberLength(optimalFiberLength);
muscle1.setTendonSlackLength(tendonSlackLength);
muscle1.setPennationAngleAtOptimalFiberLength(pennationAngle);

% Add Path points to  muscle 1
muscle1.addNewPathPoint('muscle1-point1', model.getGround(), Vec3(0.2,0,0));
muscle1.addNewPathPoint('muscle1-point2', body1, Vec3(0,0.2,0));

% Instantiate Muscle 2
muscle2 = Thelen2003Muscle();
muscle2.setName('muscle2');
muscle2.setMaxIsometricForce(maxIsometricForce)
muscle2.setOptimalFiberLength(optimalFiberLength)
muscle2.setTendonSlackLength(tendonSlackLength)
muscle2.setPennationAngleAtOptimalFiberLength(pennationAngle)

% Add Path points to muscle 2
muscle2.addNewPathPoint('muscle2-point1', model.getGround(), Vec3(-0.2,0,0));
muscle2.addNewPathPoint('muscle2-point2', body1, Vec3(0,0.2,0));

% Instantiatemuscle 3
muscle3 = Thelen2003Muscle();
muscle3.setName('muscle3');
muscle3.setMaxIsometricForce(maxIsometricForce)
muscle3.setOptimalFiberLength(optimalFiberLength)
muscle3.setTendonSlackLength(tendonSlackLength)
muscle3.setPennationAngleAtOptimalFiberLength(pennationAngle)

% Add Path points to muscle 3
muscle3.addNewPathPoint('muscle3-point1', body1, Vec3(0.0, halfheight/2,0));
muscle3.addNewPathPoint('muscle3-point2', body2, Vec3(0.0, halfheight*0,0));

% Instantiate muscle 4
muscle4 = Thelen2003Muscle();
muscle4.setName('muscle4');
muscle4.setMaxIsometricForce(maxIsometricForce)
muscle4.setOptimalFiberLength(optimalFiberLength)
muscle4.setTendonSlackLength(tendonSlackLength)
muscle4.setPennationAngleAtOptimalFiberLength(pennationAngle)

% Add Path points to muscle 4
muscle4.addNewPathPoint('muscle4-point1', body1, Vec3(0.0,halfheight/2,0));
muscle4.addNewPathPoint('muscle4-point2', body2, Vec3(0.0,halfheight,0));

% Instantiate muscle 5
muscle5 = Thelen2003Muscle();
muscle5.setName('muscle5');
muscle5.setMaxIsometricForce(maxIsometricForce)
muscle5.setOptimalFiberLength(optimalFiberLength)
muscle5.setTendonSlackLength(tendonSlackLength)
muscle5.setPennationAngleAtOptimalFiberLength(pennationAngle)

% Add Path points to muscle 5
muscle5.addNewPathPoint('muscle5-point1', model.getGround(), Vec3(0.2,0,0));
muscle5.addNewPathPoint('muscle5-point2', body2, Vec3(0.0, halfheight*0,0));

% Instantiate muscle 6
muscle6 = Thelen2003Muscle();
muscle6.setName('muscle6');
muscle6.setMaxIsometricForce(maxIsometricForce)
muscle6.setOptimalFiberLength(optimalFiberLength)
muscle6.setTendonSlackLength(tendonSlackLength)
muscle6.setPennationAngleAtOptimalFiberLength(pennationAngle)

% Add Path points to muscle 6
muscle6.addNewPathPoint('muscle6-point1', model.getGround(), Vec3(-0.2,0,0));
muscle6.addNewPathPoint('muscle6-point2', body2, Vec3(0.0,halfheight,0));

% Add the two muscles (as forces) to the model
model.addForce(muscle1);
model.addForce(muscle2);
model.addForce(muscle3);
model.addForce(muscle4);
model.addForce(muscle5);
model.addForce(muscle6);

% Finalize and save the model in an .osim file
model.finalizeConnections();
model.print('arm26.osim');
